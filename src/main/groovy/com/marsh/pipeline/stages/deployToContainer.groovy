//package com.marsh.pipeline.stages

def sayHello(def NAME){
    println("hello mr....   ")
    sh "date"
    try {
    sh ''' 
   
        echo "TRACKER || Checking If There is Any Running Container"
        if [[ `docker ps  --format "{{.Names}}" | wc -l` -ne 0 ]]; then
            echo "we have a few containers running"
            docker ps --format "{{ .Names }}"
        else
                echo "no container are running"
        fi
        echo -e "\n\n\n"

        docker images 
    '''
    } catch (Exception E ){
        println "there is an issue: Err"
        println ("pipeline failed in sayHello step.")
        //currentBuild.result = "FAILURE"
    }
    
}

return this;
